import { OnInit, Component } from '@angular/core';
import { HttpService} from '../../services/http.service';

import { NavController } from 'ionic-angular';
import{ Troubleshoot_2Page } from '../troubleshoot-2/troubleshoot-2';


@Component({
    selector: 'page-troubleshoot',
    templateUrl: 'troubleshoot.html'
  })
  export class TroubleshootPage implements OnInit {


    deviceIs: any;
    data: any;
    tabVisibl: any;
    all: any;

    show: any;
    showSib: any;

    backAllBtn: any;

    Printer: any;
    Hardware: any;
    Software: any;
    Mobile: any;
    Troubleshoot: any;
    actTabClass: any;
    childA: Array<{cat: string, txt: string}>;

    siblingStatus = 'PrinterList_2_all';

    productLevel1List: Array<{title: string, ref_id: string}>;
    productLevel2List: Array<{title: string}>;

    lang = sessionStorage.getItem('lang');

        constructor(
            private _HttpService: HttpService,
            private httpService: HttpService,
            public navCtrl: NavController
        ) {}

    ngOnInit(): void {
      this.tabVisibl = false;
      // this.tabVisibl = 'guid2';
      this.all = true;
      this.backAllBtn = false;

      this.productLevel1List = [];
      this.productLevel2List = [];

      this.onSubmit();

      this.GetHeadings();

// this.siblingStatus = 'PrinterList_2_all';

    }

    subOpt(event)
    {
      // this.childA = [
      //   {cat : 'Troubleshoot', txt : 'Paper'},
      //   {cat : 'Troubleshoot', txt : 'Other errors'},
      //   {cat : 'Troubleshoot', txt : 'Black Ink'},
      //   {cat : 'Troubleshoot', txt : 'Black and Color Ink'},
      //   {cat : 'Troubleshoot', txt : 'Color ink'}
      // ]

      // this.childA.push(
      //   {cat : 'Troubleshoot', txt : 'Paper'},
      //   {cat : 'Troubleshoot', txt : 'Other errors'},
      //   {cat : 'Troubleshoot', txt : 'Black Ink'},
      //   {cat : 'Troubleshoot', txt : 'Black and Color Ink'},
      //   {cat : 'Troubleshoot', txt : 'Color ink'}
      // );

      console.log(event);

      if(event.srcElement.className == 'img-circle')
      {
        this.actTabClass = event.srcElement.parentElement.className;
      }
      else
      {
        this.actTabClass = event.srcElement.className;
      }

      if(this.actTabClass == 'Printer')
      {
      this.Printer = true;
        this.Software = false;
        this.Hardware = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Hardware')
      {
      this.Hardware = true;
        this.Software = false;
        this.Printer = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Software')
      {
        this.Software = true;
        this.Hardware = false;
        this.Printer = false;
        this.Mobile = false;
        this.Troubleshoot = false;
      }
      else if(this.actTabClass == 'Mobile')
      {
        this.Mobile = true;
        this.Hardware = false;
        this.Printer = false;
        this.Software = false;
        this.Troubleshoot = false;
      }
      // else{
      //   this.backAllBtn = false;
      // }
      else if(this.actTabClass == 'Troubleshoot')
      {
        this.Troubleshoot = true;
        this.Hardware = false;
        this.Printer = false;
        this.Mobile = false;
        this.Software = false;
      }


      if(this.Printer == true || this.Hardware == true || this.Software == true || this.Mobile == true || this.Troubleshoot == true)
      {
        this.all = false;
        this.backAllBtn = true;
      }
      else
      {
        this.all = true;
        this.backAllBtn = false;
      }

    }

    backAll()
    {
      this.all = true;

      this.Printer = false;
      this.Hardware = false;
      this.Software = false;
      this.Mobile = false;
      this.Troubleshoot = false;
      this.backAllBtn = false;
      // this.Printer, this.Hardware, this.Software, this.Mobile, this.Troubleshoot, this.backAllBtn = false;
    }

    onSubmit() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));
// http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=1&lang=1
      return this.httpService.get('thirdlevel.json?secondlevel=2&lang=1').then(
        (success) => {
          // console.log(success.model_number);
          if (success) {
            console.log(success);
            // this.android_url = success.android_playstore_url;
            // this.ownersManual = success.downloads[1].owners;
            // this.quickstartGuide = success.downloads[1].quickstart;
            // this.GoogleCloud = success.google_cloud_url;
            // this.AirPrintUrl = success.about_print_url;
            // this.WarrantySafety = success.downloads[1].warranty_safety_sheet;
            for (var i = 0; i < success.length; i++) {
console.log( success[i]);
              this.productLevel2List.push({ title: success[i].level_details });
            }
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }

    GetHeadings() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));

      console.log(this.lang);

      return this.httpService.get('firstlevel.json?prodid=1&lang=' + this.lang ).then(
        (success) => {
          if (success) {
console.log(success.records[2].level_details);
            for (var i = 0; i < success.total_pages; i++) {

              this.productLevel1List.push({ title: success.records[i].level_details, ref_id: success.records[i].ref_id });
            }
            console.log(this.productLevel1List);
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }

    onclick(new_title, ref_id){
    this.navCtrl.push(Troubleshoot_2Page,{'new_title': new_title, 'ref_id': ref_id});
}

  }
