import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpService} from '../../services/http.service';
import{ FaqsPage } from '../faqs/faqs';

/**
 * Generated class for the Troubleshoot_3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-troubleshoot-3',
  templateUrl: 'troubleshoot-3.html',
})
export class Troubleshoot_3Page {
  new_title: string = this.navParams.get('new_title');
  productLevel2List: Array<{title: string, New_id3: string}>;
  // productLevel4List: Array<{title: string, status: Number}>;
  title_3: string;
  productLevel4ListTitle: any = ['faqstatus','videostatus','vchatstatus','driverstatus','achatstatus','callstatus'];
  faqstatus: string;
  videostatus: string;
  vchatstatus: string;
  driverstatus: string;
  achatstatus: string;
  callstatus: string;
  productLevel3List: Boolean = true;
  productLevel4List: Boolean = false;

  constructor(private httpService: HttpService, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Troubleshoot_3Page');
  }

  ngOnInit(): void {
    this.productLevel2List = [];
    // this.productLevel4List = [];

    this.onSubmit();
    // this.subLevel4();
  }
  // http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=4&lang=1

  onSubmit() {

  return this.httpService.get('thirdlevel.json?secondlevel='+this.navParams.get('level_id3')+'&lang=1').then(

    // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
      (success) => {
        // console.log(success.model_number);
        if (success) {
          console.log(success);
          if(success.length >= 1)
          {
            for (var i = 0; i < success.length; i++) {
              this.productLevel2List.push({ title: success[i].level_details, New_id3: success[i].level_id });
              console.log(' level_id_3 is ' + success[i].level_id);
            }
          }
          else{
            this.productLevel2List.push({ title: 'No record found', New_id3: 'No id' });
          }

  console.log(this.productLevel2List);
          // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      })
      .catch(
      (err) => {
        console.log(err);
      }
      )
  }


  subLevel4(level_id3New) {

console.log(level_id3New);
localStorage.setItem('level_id3New', level_id3New);

  return this.httpService.get('smartchannels.json?third_level='+level_id3New+'&num_id='+localStorage.getItem('prod_num_id')+'&lang='+sessionStorage.getItem('lang')).then(

    // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
      (success) => {
        // console.log(success.model_number);
        if (success) {
          this.faqstatus = success[0]['faqstatus'];
          this.videostatus = success[0]['videostatus'];
          this.vchatstatus = success[0]['vchatstatus'];
          this.driverstatus = success[0]['driverstatus'];
          this.achatstatus = success[0]['achatstatus'];
          this.callstatus = success[0]['callstatus'];

          if(success.length >= 1)
          {
            // for (var i = 0; i < 5; i++) {
            //   console.log(this.productLevel4ListTitle[i]);
            //   this.productLevel4List.push({ title: this.productLevel4ListTitle[i], status: success[i][this.productLevel4ListTitle[i]] });
            // }
          }
          else{
            // this.productLevel4List.push({ title: 'No record found', status: 0 });
          }

  console.log(this.productLevel4List);
          // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
        } else {
          console.log('Error in success');
        }
      })
      .catch(
      (err) => {
        console.log(err);
      }
      )
  }

  level_id4(title_3, level_id3New) {
      this.title_3 = title_3;
      this.productLevel4List = true;
    this.subLevel4(level_id3New);
  }

  levelTo5() {
      this.navCtrl.push(FaqsPage,{'level_id3': this.navParams.get('new_id')});
  }

  pushBack() {
    this.navCtrl.pop();
  }

}
