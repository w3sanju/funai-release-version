import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
// import { Headers, RequestOptions } from '@angular/http';
import { FormGroup, FormControl, Validators, FormBuilder, } from '@angular/forms';
import { Platform } from 'ionic-angular';
import { HttpService} from '../../services/http.service';
import { AlertController } from 'ionic-angular';

@Component({
    selector: 'page-download',
    templateUrl: 'download.html'
  })
  export class DownloadPage implements OnInit {
    registerForm: FormGroup;
    firstName: any;
    // lastName: any;
    emailAddress: any;
    F_os_name: any;
    F_description: any;
    F_version_name: any;
    F_download_type: any;
    sucessMessage : any;

    deviceIs: any;
    data: any;
    tabVisibl: any;
    all: any;


    supportPrinterHidden: boolean = true;

    show: any;
    showSib: any;

    backAllBtn: any;

    Drivers: any;
    Firmware: any;
    Manual: any;
    Connect: any;
    Download: any;
    downloadDriver: any;
    downloadFirmware: any;

    actTabClass: any;

    android_url: any;
    ios_url: any;
    ownersManual: any;
    quickstartGuide : any;
    GoogleCloud: string;
    AirPrintUrl: string;
    WarrantySafety: string;
    // OSTypes: any;

    lang = sessionStorage.getItem('lang');
    downloadH1: string;
    downloadH2: string;
    downloadH3: string;
    downloadH4: string;
    downloadH5: string;
    downloadSub1: string;
    downloadSub2: string;
    downloadSub3: string;
    downloadSub4: string;
    downloadSub5: string;
    downloadSub6: string;
    downloadSub7: string;

    downloadName: any;
    myOs: any;
    os_name: any;
    description: any;
    version_name: any;
    download_type: any;

    agreeTC: Boolean;

    supported_deviceList: any;

    langList: Array<{ lgName: string, lgID: number, flag:string }>;
    getFile_path: any;


    currentCountry;
    countryClass = sessionStorage.getItem('langClass');


    constructor(
      public alertCtrl: AlertController,
      private _HttpService: HttpService,
      private fb: FormBuilder,
      public plt: Platform,
      private httpService: HttpService,
      public navCtrl: NavController,
      public navParams: NavParams
      // private router: Router
    )
    { }

    productPics: Array<{title: string, modelPic: string}>;
    OSTypes: Array<{title: string, osId: string}>;
    downloadTypes: string;

    ngOnInit(): void {


      this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
      { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
      { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
      { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
      { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

      console.log(this.lang);

      this.onSubmit();
      this.productPics = [];
      this.OSTypes = [];
      this.tabVisibl = false;
      // this.tabVisibl = 'guid2';
      this.all = true;
      this.backAllBtn = false;

      if (this.plt.is('iphone')) {
        this.deviceIs = 'iphone';
        console.log('I am an iOS device!');
      }
      else if(this.plt.is('android'))
      {
        console.log('I am not iOS device!');
        this.deviceIs = 'Android';

      }

      this.getContent();
      this.getOsDetails();

      this.agreeTC = false;

      this.createFormControls();
      this.createForm();

    }

    createForm() {
      this.registerForm = new FormGroup({
        firstName: this.firstName,
        emailAddress: this.emailAddress,
        // myOs: this.myOs
      });
    }

    createFormControls(){
      this.firstName = new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+')]);
      this.emailAddress = new FormControl(null, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]);
    }

    SubmitDownloadMail(myOs,mydescription,myversion_name,mydownload_type,myfilePath){
console.log(mydownload_type);
      if (this.registerForm.valid) {
        var data = this.registerForm.value;
        data= {
          // "first_name": data.firstName,
          // "email": data.emailAddress,
          "os_name": myOs,
          "Description": mydescription,
          "version_name": myversion_name,
          "download_type": mydownload_type,
          // "file_path": "http://www.funaihelp.com/home/file_download/media-uploads-drivers-7/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe",
          "file_path": "http://www.funaihelp.com/home/file_download/media-uploads-drivers-7/KODAKVERITE-PREINSTALLER-v1.0.0.2-170413.exe/" + myfilePath,

          "user_email": data.emailAddress,
          "user_name": data.firstName
        }
        console.log(data);

        return this.httpService.put('send_download/', data).then((success) => {
          // console.log(success.data);
          console.log(success);
          this.registerForm.reset();
          if (success.status == 200) {
            this.registerForm.reset();
            this.sucessMessage = "SubmitDownloadMail successful."
          }
        }).catch(
            (err)=> {
              console.log(err);
        })
      }
    }

    showAlert() {
      console.log('showAlert')
    const alert = this.alertCtrl.create({
      title: 'TERMS AND SERVICES',
      subTitle: 'License Agreement is a legal agreement between you (either an individual or a single entity) and Funai Electric Co., Ltd. and its subsidiary companies (individually and collectively "Funai") that, to the extent your Funai product or Software Program is not otherwise subject to a written software license agreement between you and Funai or its suppliers, governs your use of any Software Program installed on or provided by Funai for use in connection with your Funai product. The term "Software Program" includes machine-readable instructions, audio/visual content (such as images and recordings), and associated media, printed materials and electronic documentation, whether incorporated into, distributed with or for use with your Funai product.',
      buttons: ['OK']
    });
      alert.present();
    }

    showDevicelist() {
    const alert = this.alertCtrl.create({
      title: 'Supported devices',
      message: this.supported_deviceList,
      buttons: ['OK']
    });
      alert.present();
    }

    setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      this.navCtrl.setRoot(this.navCtrl.getActive().component);


      this.currentCountry = this.langList.filter(c => {
        if (c.lgID == val) {
          return c;
        }
      });
      console.log(this.currentCountry[0].flag);
      this.countryClass = this.currentCountry[0].flag;
      sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    }

    // tabVisibl()
    // {
    //   return false;
    // }

    subOpt(event)
    {


      if(event.srcElement.className == 'img-circle')
      {
        this.actTabClass = event.srcElement.parentElement.className;
      }
      else
      {
        this.actTabClass = event.srcElement.className;
      }

      if(this.actTabClass == 'Download')
      {
      this.Download = true;
        this.Manual = false;
        this.Connect = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Connect')
      {
      this.Connect = true;
        this.Manual = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Manual')
      {
        this.Manual = true;
        this.Connect = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = false;
        this.agreeTC = true;
      }
      else if(this.actTabClass == 'Drivers')
      {
        this.Manual = false;
        this.Connect = false;
        this.Download = false;
        this.Drivers = true;
        this.Firmware = false;
        this.agreeTC = false;
      }
      else if(this.actTabClass == 'Firmware')
      {
        this.Manual = false;
        this.Connect = false;
        this.Download = false;
        this.Drivers = false;
        this.Firmware = true;
        this.agreeTC = false;
      }
      // else{
      //   this.backAllBtn = false;
      // }


      if(this.Manual == true || this.Connect == true || this.Download == true || this.Drivers == true || this.Firmware == true)
      {
        this.all = false;
        this.backAllBtn = true;
      }
      else
      {
        this.all = true;
        this.backAllBtn = false;
      }

    }

    backAll()
    {
      this.all = true;

      this.Manual = false;
      this.Connect = false;
      this.Download = false;
      this.Drivers = false;
      this.Firmware = false;
      this.backAllBtn = false;
      this.downloadDriver = false;
      this.downloadFirmware = false;
      this.agreeTC = false;
    }

    getContent() {

  console.log(sessionStorage.getItem('lang'));
      return this.httpService.get('static_content?lang_id=' + this.lang ).then(
        (success) => {
          if (success) {
            this.downloadH1 = success.downloads_drivers_firmwares_softwares_tab_headings;
            this.downloadH2 = success.downloads_firmware_update_tab_headings;
            this.downloadH3 = success.downloads_manuals_tab_headings;
            this.downloadH4 = success.downloads_connect_your_mobile_device_tab_headings;
            this.downloadH5 = success.downloads_warranty_safety_sheets_tab_download_warranty_safety_sheets_text;

            this.downloadSub1 = success.downloads_download_manual_tab_owners_manual;
            this.downloadSub2 = success.downloads_download_manual_tab_quickstart_guide;
            this.downloadSub3 = success.downloads_connect_your_device_tab_connect_your_android_mobile_device_to_the_printer_text;
            this.downloadSub4 = success.downloads_connect_your_device_tab_connect_your_apple_mobile_device_to_the_printer_text;
            this.downloadSub5 = success.downloads_connect_your_device_tab_learn_about_airprint_text;
            this.downloadSub6 = success.downloads_connect_your_device_tab_learn_about_google_cloud_print_text;
            this.downloadSub7 = success.downloads_warranty_safety_sheets_tab_download_warranty_safety_sheets_text;


          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }


    onSubmit() {

      // var tokenId = localStorage.getItem('token_array');
      // var tokenId = this.navParams.get('token_array');

      console.log(localStorage.getItem('prod_num_id'));

      return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
        (success) => {
          console.log(success.model_number);
          if (success) {
            console.log(success);
            this.android_url = success.android_playstore_url;
            this.ios_url = success.ios_playstore_url;
            this.ownersManual = success.downloads[1].owners;
            this.quickstartGuide = success.downloads[1].quickstart;
            this.GoogleCloud = success.google_cloud_url;
            this.AirPrintUrl = success.about_print_url;
            this.WarrantySafety = success.downloads[1].warranty_safety_sheet;

            this.supported_deviceList = success.supported_device;

            // this.OSTypes.push(success.firmwares);
            // console.log(success.firmwares[1])

            // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }


    getOsDetails() {
      // return this.httpService.get('drivers?lang_id=' + this.lang + '&numid='+localStorage.getItem('prod_num_id')).then(
      return this.httpService.get('drivers?lang_id=1&numid='+localStorage.getItem('prod_num_id')).then(

        (success) => {
          if (success) {
            // console.log(success);
            for (var i = 0; i < success.length; i++) {
                this.OSTypes.push({title: success[i].os_name, osId: success[i].os_id});
            }
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }


    downloadDetails(myId, typeof_download) {
      // return this.httpService.get('version_types?download_type=drivers&status=1&lang_id=' + this.lang + '&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(

      // return this.httpService.get('version_types?download_type=drivers&status=1&lang_id=1&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(

      return this.httpService.get('version_types?download_type=' + typeof_download + '&status=1&lang_id=' + sessionStorage.getItem("lang") + '&id=' + myId + '&numid='+localStorage.getItem('prod_num_id')).then(


        (success) => {
          if (success) {
            for (var i = 0; i < success.length; i++) {
                this.downloadName = success[i].drivers;
                this.downloadTypes = success[i].description;

                this.os_name = success[i].os_name;
                this.description = success[i].description;
                this.version_name = success[i].version_name;
                this.download_type = typeof_download;
                if(success[i].drivers)
                {
                  this.getFile_path = success[i].drivers;
                }
                else if(success[i].firmware)
                {
                  this.getFile_path = success[i].firmware;
                }
                else{
                  this.getFile_path = 'Path not available';
                }


                console.log('drivers - ' + success[i].drivers);
            }
          } else {
            console.log('Error in success');
          }
        })
        .catch(
        (err) => {
          console.log(err);
        }
        )
    }

    // http://124.30.44.230/funaihelpver1/api/ver1/drivers?numid=1&lang_id=1



  }
