import 'rxjs/add/operator/switchMap';
import { Component, OnInit, ViewChild } from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Nav, NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
// import { Component, ViewChild } from '@angular/core';
// import { Nav, NavController } from 'ionic-angular';
// import { HttpClient } from '@angular/common/http';
import { TabsPage } from './../tabs/tabs';
import { RegisterPage } from './../register/register';
import { CategorySelectionPage } from '../category-selection/category-selection';
// import { ListPage } from './../list/list';
// import { Headers, RequestOptions } from '@angular/http';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

// @Injectable()
export class HomePage implements OnInit {

  public langName: string = "US spanish";

  countryName: any;
  currentCountry;
  countryClass: any;


  selectOptions: any = {
    title: 'Language',
    subTitle: 'Select your toppings',
    mode: 'md',
    cssClass: 'my-class'
  };

  data: any;
  constructor(
    private _HttpService: HttpService,
    public plt: Platform,
    private httpService: HttpService,
    public navCtrl: NavController
    // private router: Router
  ) { }

  vals: any;

  lang: any;
  langList: Array<{ lgName: string, lgID: number, flag: string }>;

  tabsPage = TabsPage;
  productPics: Array<{ title: string, modelPic: string, prod_num_id: string }>;
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  osWat: any;


  // anOtherPage: CategorySelectionPage;
  goAnOtherPage(tokenNumber, prod_num_id) {
    // console.log(tokenNumber);
    // localStorage.clear();
    localStorage.setItem('token_array', tokenNumber);
    localStorage.setItem('prod_num_id', prod_num_id);
    // localStorage.getItem('lang');

    //   this.navCtrl.setRoot(CategorySelectionPage);

    // this.navCtrl.setRoot(CategorySelectionPage,{'token_array': tokenNumber});
    // this.navCtrl.setRoot(CategorySelectionPage,{'token_array': tokenNumber});

    this.navCtrl.push(CategorySelectionPage, { 'token_array': tokenNumber });



    // this.navCtrl.push(CategorySelectionPage,{'token_array': tokenNumber});
  }

  // private options: RequestOptions;

  // constructor(public platform: Platform, public navCtrl: NavController, private http: HttpClient) {
  // this.productPics = [
  //   { title: 'https://picsum.photos/240/150/'},
  //   { title: 'https://picsum.photos/240/150/'},
  //   { title: 'https://picsum.photos/240/150/'},
  //   { title: 'https://picsum.photos/240/150/'}
  // ];
  // }



  ngOnInit(): void {

    this.vals = '';

    // this.myGet();
    // this.myPost();

    // this.myPost(this.vals);

    this.onSubmit();

    if (!localStorage.getItem('lang')) {
      // sessionStorage.setItem('lang', '1');
      console.log('no lang');
      this.countryClass = 'us';
    }
    else {
      console.log('got lang');
    }


    this.productPics = [];

    this.langList = [{ lgName: 'United States (English)', lgID: 1, flag: 'us' },
    { lgName: 'Canada (French)', lgID: 109, flag: 'canada' },
    { lgName: 'United States (Spanish)', lgID: 111, flag: 'us' },
    { lgName: 'Canada (English)', lgID: 110, flag: 'canada' },
    { lgName: 'United Kingdom (English)', lgID: 108, flag: 'uk' }];

    if (this.plt.is('iphone')) {
      this.osWat = 'iphone';
      //console.log('I am an iOS device!');
    }
    else {
      //console.log('I am not iOS device!');
      this.osWat = 'Not iphone';
    }

  // }
  // if(sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "")
  // {
  //
  //   this.lang = sessionStorage.getItem('lang');
  //   this.countryName = sessionStorage.getItem('lang');
  //   console.log(this.countryName);
  // }
  // else
  // {
  //   this.lang = 1;
  //
  //
  //   this.lang = sessionStorage.setItem('lang', '1');
  //   this.countryName = 1;
  //   console.log(this.countryName);
  // }




if (sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "") {
  this.lang = sessionStorage.getItem('lang');
  this.countryName = sessionStorage.getItem('lang');
  this.countryClass = sessionStorage.getItem('langClass');
  console.log(this.countryName);
}
else {
  this.lang = sessionStorage.setItem('lang', '1');
  this.countryClass = 'us';
  sessionStorage.setItem('langClass','us');
  this.countryName = 1;
  console.log(this.countryName);
}




    // sessionStorage.setItem('lang', '1');

  }


  openPage(page) {
    this.nav.setRoot(page.component);

  }

  pushRegister() {
    // this.navCtrl.push(HomePage);
    this.navCtrl.setRoot(RegisterPage);
  }

  setLang(val: any) {
    console.log(val);
    sessionStorage.setItem('lang', val);
    localStorage.setItem('lang', val);

    // this.navCtrl.setRoot(HomePage);


    this.currentCountry = this.langList.filter(c => {
      if (c.lgID == val) {
        return c;
      }
    });
    console.log(this.currentCountry[0].flag);
    this.countryClass = this.currentCountry[0].flag;
    sessionStorage.setItem('langClass', this.currentCountry[0].flag);
    //this.navCtrl.push(this.navCtrl.getActive().component);
  }


  // private handleError(error: any): Promise<any> {
  //     console.error('An error occurred', error);
  //     return Promise.reject(error.message || error);
  // }

  // myGet()
  // {
  //   this.configure();
  //   this.http.get('http://124.30.44.230/funaihelpver1/api/ver1/productlist.json?compid=1&prodid=1&lang=1').subscribe(padamData => {
  //     console.log(padamData);
  //   },
  //     err => {
  //       console.log("Error occured.");
  //     });
  // }


  // Working Post API

  // myPost()
  // {
  //   this.http.post('https://httpbin.org/post', { })
  //   .subscribe(
  //      (val) => {
  //          console.log("POST call successful value returned in body", val);
  //      },
  //      response => {
  //          console.log("POST call in error", response);
  //      },
  //      () => {
  //          console.log("The POST observable is now completed.");
  //      });
  // }



  // this.http.post('https://httpbin.org/post', {
  //     "Username": "funaiadmin",
  //     "Password": "Fu@dm!n45"
  // })


  // http://124.30.44.230/funaihelpver1/api/productregistration

  extractData(res: Response) {
    let body = res.json();
    console.log(body);
    return body || {};
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  onSubmit() {

    return this.httpService.get('productlist.json?compid=1&prodid=1&lang=109').then(

      // return this.httpService.get('countries.json').then(

      // return this.httpService.get('users').then(

      // return this.httpService.get('latest').then(
      (success) => {
        console.log(success.model_info[1]);
        // if(success.data.isSucceeded == true){
        //   console.log(success.data);
        //
        // }
        if (success) {
          // console.log(success);
          // this.productPics.push({title : success.model_info[2].num_id});

          for (var i = 0; i < success.model_info.length; i++) {
            // this.productPics.push({title : success.model_info[2]});
            this.productPics.push({ title: success.model_info[i].model_number, modelPic: success.model_info[i].img, prod_num_id: success.model_info[i].num_id });
          }

          console.log(this.productPics);

        } else {
          console.log('Error in success');
        }
      })
      .catch(
        //used Arrow function here
        (err) => {
          console.log(err);
          // this.router.navigate(['/']);
        }
      )
  }


}
