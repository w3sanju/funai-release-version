import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HttpService} from '../../services/http.service';
import{ Troubleshoot_3Page } from '../troubleshoot-3/troubleshoot-3';

/**
 * Generated class for the Troubleshoot_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-troubleshoot-2',
  templateUrl: 'troubleshoot-2.html',
})
export class Troubleshoot_2Page {

  productLevel2List: Array<{title: string, level_id3: string}>;
  new_title: string = this.navParams.get('new_title');

  lang = sessionStorage.getItem('lang');

  constructor(private httpService: HttpService, public navCtrl: NavController, public navParams: NavParams)
  { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Troubleshoot_2Page');
  }
ngOnInit(): void {

  console.log(this.navParams.get('new_id'));
  this.productLevel2List = [];
  this.onSubmit();
}
// http://124.30.44.230/funaihelpver1/api/ver1/secondlevel.json?firstlevel=4&lang=1
onSubmit() {

  // var tokenId = localStorage.getItem('token_array');
  // var tokenId = this.navParams.get('token_array');
  console.log(this.navParams.get('ref_id'));

// this.new_title = this.navParams.get('new_title');

return this.httpService.get('secondlevel.json?firstlevel=' + this.navParams.get('ref_id') + '&lang=' + this.lang).then(

  // return this.httpService.get('downloads.json?lang=1&numid='+localStorage.getItem('prod_num_id')).then(
    (success) => {
      console.log(success.model_number);
      if (success) {
        console.log(success);
        for (var i = 0; i < success.length; i++) {
console.log( success[i]);
          this.productLevel2List.push({ title: success[i].level_details, level_id3: success[i].level_id });
        }
console.log(this.productLevel2List);
        // this.productPics.push({ title: success.model_info[tokenId].model_number, modelPic: success.model_info[tokenId].img });
      } else {
        console.log('Error in success');
      }
    })
    .catch(
    (err) => {
      console.log(err);
    }
    )
}

onclick(new_id, level_id3, new_title){
this.navCtrl.push(Troubleshoot_3Page,{'new_id': new_id, level_id3: level_id3, 'new_title': new_title});
}

pushBack() {
  this.navCtrl.pop();
}

}
