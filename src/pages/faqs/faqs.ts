import { Component } from '@angular/core';
import { NavController, NavParams, } from 'ionic-angular';
import { HttpService } from './../../services/http.service';

@Component({
    selector: 'page-faqs',
    templateUrl: 'faqs.html',
})

export class FaqsPage {

    faqs :any;

    constructor(
        private _HttpService: HttpService,
        public navCtrl: NavController,
        public navParams: NavParams,
        private httpService: HttpService,
    ){

    }

    ngOnInit(){
        this.faqsData();
    }
    faqsData(){
        return this.httpService.get('faqs.json?num_id='+localStorage.getItem('prod_num_id')+'&thirdlevel_id='+localStorage.getItem('level_id3New')+'&lang=1').then((success) => {
            console.log(success);
            // this.faqs = success.faqs.replace('src="/media/', 'src="http://124.30.44.230/funaihelpver1/media/');
            this.faqs = success.faqs.split('src="/media/').join('src="'+this._HttpService.srcUrl_124+'/media/').split('<div><a href="#" onclick="window.close(); return false;">CLOSE</a></div>').join('');

            console.log(this.faqs);
            }).catch(
              (err)=> {
                console.log(err);
        })
    }
    toggleSection(i) {
        this.faqs[i].open = !this.faqs[i].open;
    }
    toggleItem(i, j) {
        this.faqs[i].children[j].open = !this.faqs[i].children[j].open;
    }
}
