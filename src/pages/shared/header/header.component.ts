import { Component } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';
import {HomePage } from './../../home/home';
import { RegisterPage } from './../../register/register';



@Component({
    selector: 'page-header',
    templateUrl: 'header.html'
  })
  export class HeaderPage {
    // lang = sessionStorage.getItem('lang');
    lang: any;
    gender: any;

    langList: Array<{ lgName: string, lgID: number }>;

           constructor(public navCtrl: NavController,
           public navParams: NavParams) {

        }

    ngOnInit(): void {
      // if(!sessionStorage.getItem('lang'))
      if(sessionStorage.getItem("lang") !== null && sessionStorage.getItem("lang") !== "")
      {
        this.lang = 1;
        this.gender = 1;
        console.log(this.gender);
      }
      else
      {
        this.lang = sessionStorage.getItem('lang');
        this.gender = sessionStorage.getItem('lang');
        console.log(this.gender);
      }
      this.langList = [{ lgName: 'United States (English)', lgID: 1 },{ lgName: 'Canada (French)', lgID: 109 },{ lgName: 'United States (Spanish)', lgID: 111 },{ lgName: 'Canada (English)', lgID: 110 },{ lgName: 'United Kingdom (English)', lgID: 108 }];
    }

    setLang(val: any) {
      console.log(val);
      sessionStorage.setItem('lang', val);
      this.navCtrl.push(this.navCtrl.getActive().component);
    }

    pushHome() {
      // this.navCtrl.push(HomePage);
      this.navCtrl.setRoot(HomePage);
    }
    pushRegister() {
      // this.navCtrl.push(HomePage);
      this.navCtrl.setRoot(RegisterPage);
    }

  }
