import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
// import { ListPage } from '../pages/list/list';
// import { CategorySelectionPage } from '../pages/category-selection/category-selection';
// import { FeedbackPage } from '../pages/feedback/feedback';
import{ RegisterPage } from '../pages/register/register';
// import { FooterPage } from '../pages/shared/footer/footer.component';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  @ViewChild(Nav) nav: Nav;

public srcUrl = "http://www.funaihelp.com/media/uploads/";

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  public langName: string = "US spanish";

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      // { title: 'List', component: ListPage },
      // { title: 'FeedbackPage', component: FeedbackPage },
      // { title: 'CategorySelectionPage', component: CategorySelectionPage },
      { title: 'Register', component: RegisterPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
