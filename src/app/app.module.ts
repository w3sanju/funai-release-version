import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HttpService } from '../services/http.service';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { FeedbackPage } from '../pages/feedback/feedback';
// import { FooterNavPage } from '../pages/footer-nav/footer-nav';

import { CategorySelectionPage } from '../pages/category-selection/category-selection';
import { TabsPage } from '../pages/tabs/tabs';
import{ DownloadPage } from '../pages/download/download';
import{ TroubleshootPage } from '../pages/troubleshoot/troubleshoot';
import{ Troubleshoot_2Page } from '../pages/troubleshoot-2/troubleshoot-2';
import{ Troubleshoot_3Page } from '../pages/troubleshoot-3/troubleshoot-3';

import{ RegisterPage } from '../pages/register/register';
import{ FaqsPage } from '../pages/faqs/faqs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FooterPage } from '../pages/shared/footer/footer.component';

import { HeaderPage } from '../pages/shared/header/header.component';


// const routes: Routes = [ 
//   { path: 'register', component: RegisterPage, data: { title: 'Register' } }, 
// ];

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    FeedbackPage,
    TabsPage,
    DownloadPage,
    TroubleshootPage,
    Troubleshoot_2Page,
    Troubleshoot_3Page,
    CategorySelectionPage,
    FaqsPage,
    RegisterPage,
    FooterPage,
    HeaderPage
    // FooterNavPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),    
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    FeedbackPage,
    TabsPage,
    DownloadPage,
    TroubleshootPage,
    Troubleshoot_2Page,
    Troubleshoot_3Page,
    CategorySelectionPage,
    FaqsPage,
    RegisterPage,
    FooterPage,
    HeaderPage
    // FooterNavPage
  ],
  providers: [
    HttpService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
